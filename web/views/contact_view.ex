defmodule PhoenixAndElm.ContactView do
  use PhoenixAndElm.Web, :view

  # To return the JSON structure we need in the Elm front-end.
  # The final result is a Scrivener.Page struct encoded to JSON, where the
  # entries key is a list of the Contact model struct
  def render("index.json", %{page: page}), do: page
end
