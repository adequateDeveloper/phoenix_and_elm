module View exposing (..)

import Html exposing (..)
import Model exposing (..)
import Messages exposing (..)
import Html.Attributes exposing (..)
import ContactList.View exposing (indexView)


view : Model -> Html Msg
view model =
    section []
        [ headerView
        , div []
            [ indexView model ]
        ]


headerView : Html Msg
headerView =
    header [ class "main-header" ]
        [ h1 []
            [ text "Phoenix and Elm: A real use case" ]
        ]
