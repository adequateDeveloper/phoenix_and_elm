module Main exposing (..)

import Html
import Model exposing (..)
import Update exposing (..)
import View exposing (view)
import Commands exposing (fetch)
import Messages exposing (Msg(..))


{-
   Html.program receives the init function that populates the model with the
   initial state and calls an initial command... but, what are commands?
   In Elm, if we want to do stuff like making Http requests or handling messages
   from web sockets or any other thing that has *side effects*, we need to use
   commands.
   In our case, as the model is initially populated empty, the first thing we
   have to do is to request the first page to the backend.
-}


init : ( Model, Cmd Msg )
init =
    initialModel ! [ fetch ]


main : Program Never Model Msg
main =
    Html.program
        { init = init
        , view = view
        , update = update
        , subscriptions = always <| Sub.none
        }
