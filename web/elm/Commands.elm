module Commands exposing (..)

import Http
import Messages exposing (Msg(..))
import Decoders exposing (contactListDecoder)


{-
   The fetch function calls Http.send passing the Msg to trigger when the
   request is made, updating the model with the result with the update function.
   As the Http request is going to return JSON from the backend, we need to
   transform it into something that our application understands, in this case,
   a ContactList record. Therefore, we pass the contactListDecoder to the
   request to decode the result.
-}


fetch : Cmd Msg
fetch =
    let
        apiUrl =
            "/api/contacts"

        request =
            Http.get apiUrl contactListDecoder
    in
        Http.send FetchResult request
